package main

import (
"encoding/json"
"fmt"
"github.com/gorilla/mux"
"log"
"net/http"
)

type Tasks struct {
	Id         string `json:"id"`
	TaksName   string `json:"taksName"`
	TaksDetail string `json:"taksDetail"`
	Date       string `json:"date"`
}

var tasks []Tasks

func allTasks() {
	task := Tasks{
		Id:         "01",
		TaksName:   "Course 6 Client & Server",
		TaksDetail: "Finish what you have started",
		Date:       "07-14-2022",
	}
	tasks = append(tasks, task)

	task1 := Tasks{
		Id:         "02",
		TaksName:   "Task for Materi 5",
		TaksDetail: "There is no Change",
		Date:       "07-14-2022",
	}
	tasks = append(tasks, task1)
}

func homePage(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writer, "Hello Home Page")
}

func getTasks(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Add("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(tasks)
}

func getTask(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Add("Content-Type", "application/json")
	taksId := mux.Vars(request)
	flag := false

	for i := 0; i < len(tasks); i++ {
		if taksId["id"] == tasks[i].Id {
			json.NewEncoder(writer).Encode(tasks[i])
			flag = true
			break
		}
	}
	if flag == false {
		json.NewEncoder(writer).Encode(map[string]string{"Status": "Error"})
	}
}

func createTaks(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Add("Content-Type", "application/json")
	var taskTodo Tasks
	json.NewDecoder(request.Body).Decode(&taskTodo)

	task := Tasks{
		Id:         taskTodo.Id,
		TaksName:   taskTodo.TaksName,
		TaksDetail: taskTodo.TaksDetail,
		Date:       taskTodo.Date,
	}
	tasks = append(tasks, task)
	writer.WriteHeader(200)
	json.NewEncoder(writer).Encode(&task)
}

func deleteTask(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Add("Content-Type", "application/json")
	params := mux.Vars(request)
	for index, item := range tasks {
		if item.Id == params["id"] {
			tasks = append(tasks[:index], tasks[index+1:]...)
			break
		}
	}
	writer.WriteHeader(http.StatusOK)
	_, err := writer.Write([]byte(`{"Message" : "Success delete task"}`))
	if err != nil {
		panic(err)
	}
}

func updateTask(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Add("Content-Type", "application/json")
	params := mux.Vars(request)
	for index, item := range tasks {
		if item.Id == params["id"] {
			tasks = append(tasks[:index], tasks[index+1:]...)
		}
	}
	var taks Tasks
	_ = json.NewDecoder(request.Body).Decode(&taks)
	taks.Id = params["id"]
	tasks = append(tasks, taks)
	json.NewEncoder(writer).Encode(taks)
	writer.WriteHeader(http.StatusOK)
	_, err := writer.Write([]byte(`{"message": "Success to update task"}`))
	if err != nil {
		panic(err)
	}
}

func handlerRoutes() {
	router := mux.NewRouter()
	router.HandleFunc("/", homePage).Methods("GET")
	router.HandleFunc("/api/getTasks", getTasks).Methods("GET")
	router.HandleFunc("/api/getTask/{id}", getTask).Methods("GET")
	router.HandleFunc("/api/Create", createTaks).Methods("POST")
	router.HandleFunc("/api/delete/{id}", deleteTask).Methods("DELETE")
	router.HandleFunc("/api/update/{id}", updateTask).Methods("PUT")

	log.Fatal(http.ListenAndServe("localhost:8181", router))
}

func main() {
	allTasks()
	fmt.Println("application is running...")
	handlerRoutes()
}
